var http = require("http");
var _ = require("underscore");
var config = require('../config');

var spreadsheets = config.spreadsheets;
var spreadsheetId = config.spreadsheetId;
var username = config.username;
var password = config.password;

module.exports = function(robot) {
  return robot.respond(/(planning) (.*)/i, function(msg) {
    var query = msg.match[2];
    var names = query.split(" ");
    var today = new Date();
    var date = today.getDate() + '/' + (today.getMonth() + 1);
    var Spreadsheet = require('edit-google-spreadsheet');

    for (var sheet in spreadsheets) {
      Spreadsheet.load({
        debug: true,
        spreadsheetId: spreadsheetId,
        worksheetId: spreadsheets[sheet],
        username: username,
        password: password,

      }, function sheetReady(err, spreadsheet) {
        if(err) throw err;

        spreadsheet.receive(function(err, rows, info) {
          if(err) throw err;

          var dateColumn = '';

          _.forEach(rows["2"], function(dateValue, dateIndex){
            if (dateValue == date) {
              dateColumn = dateIndex;
            }
          });

          for (var nameIndex in names) {
            name = names[nameIndex];
            var planning = '';

            _.forEach(rows, function(row, rowIndex) {
              var nameToCheck = row["1"];

              try {
                if (nameToCheck !== undefined && nameToCheck.toUpperCase() == name.toUpperCase()) {
                  msg.reply(name + ' is planned on ' + row[dateColumn]);
                }
                return;
              } catch (Exception){}
            });
          }
        });
      });
    }
  });
};
