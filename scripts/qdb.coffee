#
# Description:
#   Fetch a random quote from bash.org
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   hubot quote me
#
# Author:
#   imot3k

Util = require "util"
Select = require("soupselect").select
HTMLParser = require "htmlparser"

module.exports = (robot) ->
  robot.respond /quote( me)/i, (msg) ->
    msg.http("http://www.bash.org/?random")
      .get() (err, res, body) ->
        text = ''
        selector = ['.qt']
        quote = select_element(body, selector)
        for key, value of quote.children
          if quote.children[key].name == 'br'
            additionalText = "/n"
          else
            additionalText = quote.children[key].raw
          text = text + additionalText
        msg.send text

select_element = (body, selectors) ->
  html_handler = new HTMLParser.DefaultHandler((()->), ignoreWhitespace: true)
  html_parser = new HTMLParser.Parser html_handler

  html_parser.parseComplete body
  for selector in selectors
    quote_container = Select(html_handler.dom, selector)
    if quote_container && quote_container[0]
      return quote_container[0]
