# Description:
#   Utility commands surrounding Hubot uptime.
#
# Commands:
#   hubot time - Reply with current time
#   hubot date - Reply with current date
#   hubot die - End hubot process

module.exports = (robot) ->

  robot.respond /DATE$/i, (msg) ->
    dt = new Date()
    msg.send "Today is: #{dt.getDate()}/#{dt.getMonth()+1}/#{dt.getFullYear()}"


  robot.respond /TIME$/i, (msg) ->
    dt = new Date()
    msg.send "It is now: #{dt.getHours() + 2}:#{dt.getMinutes()}:#{dt.getSeconds()}"

  robot.respond /DIE$/i, (msg) ->
    msg.send "Goodbye, cruel world."
    process.exit 0

