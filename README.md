# Zeus

Zeus is a clone of [hubot](https://hubot.github.com/) for [Xcheck](http://www.crosscheck.be/crosscheck/)
If you want to contribute or test hubot on your local machine you can visit the [doc](https://github.com/github/hubot/blob/master/docs/README.md).

## Integrations

### bitbucket

### jira

### jenkings

### dropbox

### confluence

##Current command list of zeus

* <spotify link> - returns info about the link (track, artist, etc.)
* applause|applaud|bravo|slow clap - Get applause
* dance - Display a dancing Carlton
* Zeus <user> doesn't have <role> role - Removes a role from a user
* Zeus <user> has <role> role - Assigns a role to a user
* Zeus <user> is a badass guitarist - assign a role to a user
* Zeus <user> is not a badass guitarist - remove a role from a user
* Zeus add deadline 2011-10-30 Thing - Add a deadline for October 10, 2011
* Zeus animate me <query> - The same thing as `image me`, except adds a few parameters to try to return an animated GIF instead.
* Zeus beer me - Grab me a beer
* Zeus birthday quote for <user> -- congratulate <user> with a random birthday quote
* Zeus catfact - Reply back with random cat fact.
* Zeus celebrate me <user> -- congratulate <user> with an inspirational greeting
* Zeus cheer me up - A little pick me up
* Zeus clear deadlines - Remove all the deadlines
* Zeus date - Reply with current date
* Zeus deadlines - List what you have due
* Zeus decide "<option 1>" "<option 2>" "<option x>" - Randomly picks an option
* Zeus decide <option1> <option2> <option3> - Randomly picks an option
* Zeus die - End Zeus process
* Zeus get directions "<origin>" "<destination>" -- Shows directions between these locations
* Zeus google me <query> - Googles <query> & returns 1st result's URL
* Zeus happy birthday me <user> -- congratulate <user> with a humorous greeting
* Zeus help - Displays all of the help commands that Zeus knows about.
* Zeus help <query> - Displays all help commands that match <query>.
* Zeus i am working on <anything> - Set what you're working on
* Zeus image me <query> - The Original. Queries Google Images for <query> and returns a random top result.
* Zeus map me <query> - Returns a map view of the area returned by `query`.
* Zeus md5|sha|sha1|sha256|sha512|rmd160 me <string> - Generate hash of <string>
* Zeus mustache me <query> - Searches Google Images for the specified query and mustaches it.
* Zeus mustache me <url> - Adds a mustache to the specified URL.
* Zeus npm version <package name> - returns npm package version if it exists
* Zeus pug me - Receive a pug
* Zeus remove deadline Thing - Remove a deadline named "Thing"
* Zeus show storage - Display the contents that are persisted in the brain
* Zeus show users - Display all users that Zeus knows about
* Zeus the rules - Make sure Zeus still knows the rules.
* Zeus there's a gem for <that> - Returns a link to a gem on http://rubygems.org
* Zeus translate me <phrase> - Searches for a translation for the <phrase> and then prints that bad boy out.
* Zeus translate me from <source> into <target> <phrase> - Translates <phrase> from <source> into <target>. Both <source> and <target> are optional
* Zeus travis me <user>/<repo> - Returns the build status of https://github.com/<user>/<repo>
* Zeus tvshow me <show> - Show info about <show>
* Zeus url encode|decode <query> - URL encode or decode <string>
* Zeus url form encode|decode <query> - URL form-data encode or decode <string>
* Zeus what is everyone working on? - Find out what everyone is working on
* Zeus what role does <user> have - Find out what roles are assigned to a specific user
* Zeus whatis <term> - search the term on http://urbandictionary.com and get a random popular definition for the term.
* Zeus who <does something>? - Returns who does !
* Zeus who has admin role - Find out who's an admin and can assign roles
* Zeus who is <user> - see what roles a user has
* Zeus wiki me <query> - Searches for <query> on Wikipedia.
* Zeus youtube me <query> - Searches YouTube for the query and returns the video embed link.
* sarcastic applause|clap - Get sarcastic applause
* sloth me - Sends a sloth image URL
* Zeus time - Reply with current time
* Zeus date - Reply with current date
